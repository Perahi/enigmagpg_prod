/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

var keyusers = {

	/**
   	* Param Comment
   	*
   	* @type {string}
   	* @private
   	*/

  	getKeyUsers_: 'https://app.enigmagpg.com/api/v1/keycontainer/?format=json',

  	/**
   	* Param Comment
   	*
   	* @type {string}
   	* @private
   	*/

   	findString_: '',

   	/**
   	* Param Comment
   	*
   	* @type {array}
   	* @private
   	*/

   	show_: ['company','firstname','lastname','email'], 

   	/**
   	* Param Comment
   	*
   	* @type {array}
   	* @private
   	*/

   	keyusers_:[],

  	/**
   	* Function Comment
   	* 
   	*
   	* @public
   	*/

   	getKeyUsersByString: function(findString){
      var keyUserList = document.querySelector('#keyUserList');
      // Show spinner
      gui.showSpinnerSearchBox();
   		// Empty storage
   		this.keyusers_ = [];
		  // If findString isn't empty we are goint to fill the list
   		if(findString != ''){		
	   		// Set findString
	   		this.findString_ = findString;
	   		// Get access token from local storage
	   		this.getKeyUsersRequest_(localStorage.accessToken);	
   		}else{
   			// Hide user list
   			keyUserList.style.display = 'none';
        //Hide spinner
        gui.hideSpinnerSearchBox();
   		}
   	},


   	/**
   	* Function Comment
   	* 
   	*
   	* @param {String} An access token.
   	* @return {string} .
   	* @private
   	*/
  	
  	getKeyUsersRequest_: function(accessToken) {
    	var xmlhttp = new XMLHttpRequest();
    	xmlhttp.onload = this.parseKeyUserRequest_.bind(this);
    	xmlhttp.open("GET",this.getKeyUsers_,true);
    	xmlhttp.setRequestHeader("Authorization","OAuth "+accessToken);
    	xmlhttp.send();
  	},

	/**
   	* Function Comment
   	* 
   	*
   	* @param {String} An access token.
   	* @param {ProgressEvent} e The XHR ProgressEvent.
   	* @private
   	*/
   
  	parseKeyUserRequest_ : function(e){
  		this.emptyKeyUserList_();
  		var jsonObj = JSON.parse(e.target.responseText);
  		localStorage.keyuser = [];
  		if(jsonObj.hasOwnProperty('objects')){
  			for (var i in jsonObj.objects) {
  				this.buildSearchList_(jsonObj.objects[i]);		
  			}	
  		}
      //Hide spinner
      gui.hideSpinnerSearchBox();
  	},

  	/**
  	 * Function Comment
  	 *
  	 * @param {Javascript Object} A keyuser
  	 * 
  	 */
  	buildSearchList_:function(keyuser){

      var keyUserList = document.querySelector('#keyUserList');

  		var li,a,text = '';
  		
  		for (var property in keyuser) {
  			if(this.show_.indexOf(property)!=-1){
  			 	text += (text!='')?',':'';
      	 		text += (keyuser[property]||'')?keyuser[property]:'';
  			}
      }

    	if(text.indexOf(this.findString_)!=-1){
    		// Storage user for using then
    		this.keyusers_.push(keyuser);
    		// Show key user list
  			this.showKeyUserList_();
  			// Build a new item list from keyuser	
  			li = document.createElement('li');
  			a = document.createElement('a');
  			a.innerText = text;
  			a.href = "#";
  			a.setAttribute("data-keyuser-id", keyuser.id);
  			a.addEventListener('click',this.selectKeyUser_.bind(this));
  			li.appendChild(a);
  			keyUserList.appendChild(li);
    	}
  	},

  	/**
  	 * Function Comment
  	 *
  	 * @param {Event} A keyuser
  	 * 
  	 */

  	emptyKeyUserList_:function(){
      var keyUserList = document.querySelector('#keyUserList');
  		// Empty a key user list
   		while(keyUserList.hasChildNodes() ){
    		keyUserList.removeChild(keyUserList.lastChild);
		  }
  	},

  	/**
  	 * Function Comment
  	 *
  	 * @param {Event} A keyuser
  	 * 
  	 */

  	 selectKeyUser_ : function(e){
  	 	// Prevent default link behavior
  	 	e.preventDefault();
  	 	// Set key user 
  	 	searchBar.value = e.target.innerText;
  	 	var i = 0,keyuserId = e.target.getAttribute("data-keyuser-id");
  	 	while(i<this.keyusers_.length && (this.keyusers_[i].id!=keyuserId)){
  	 		i++;
  	 	}
  	 	if(i<this.keyusers_.length && (this.keyusers_[i].id==keyuserId)){
  	 		pubkey.innerText = this.keyusers_[i].public_key;
  	 	}
  	 	// Hide key user list
  	 	this.hideKeyUserList_();
  	 },


  	/**
  	 * Function Comment
  	 *
  	 * @param {Event} A keyuser
  	 * 
  	 */

  	 showKeyUserList_: function(){
      var keyUserList = document.querySelector('#keyUserList');
  	 	keyUserList.style.display = 'block';
  	 },


  	/**
  	 * Function Comment
  	 *
  	 * @param {Event} A keyuser
  	 * 
  	 */
  	 hideKeyUserList_: function(){
      var keyUserList = document.querySelector('#keyUserList');
  	 	// Empty key user list
  	 	this.emptyKeyUserList_();
  	 	// Hide user list
  	 	keyUserList.style.display = 'none';
  	 }

};