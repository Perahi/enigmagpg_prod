/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

document.addEventListener('DOMContentLoaded',function(){controller.start();});

var controller = {

  /**
  * Function Comment
  * 
  * @prublic
  */  
  
  start : function(){
    // Fisrt parse html an translate string
    this.transaleHTML_(document.body);
    // Then check for previous login
    login.checkSession();
    // Load HTML objects 
    this.loadHTMLObjects_();
    // Check if context menu has been checked
    this.checkContextMenu_();
  },
  
  /**
  * Function Comment
  * 
  * @private
  */  
  checkContextMenu_ : function(){
    // Fisrt get hash from the url
    var substring = window.location.hash.substring(1);
    if(substring != ''){
      // Retrieve the selected text from background page
      var selectedText = chrome.extension.getBackgroundPage().selectedText;
      var tab = (substring === 'cipher')?document.querySelector('#cipherTextTab'):document.querySelector('#decipherTextTab');
      var textArea = (substring === 'cipher')?document.querySelector('#textAreaCipher'):document.querySelector('#textAreaDecipher');
      tab.click();
      textArea.value = selectedText;
    }
  },
  


  /**
  * Function Comment
  * 
  * @private
  */  
  transaleHTML_ : function(root){
    var regExp = /\{{([^)]+)\}}/;
    var matches, string, substring;
    if(root.hasOwnProperty('innerText')){
      string = root.innerText.replace(/ /g,'').replace(/(\r\n|\n|\r)/gm,'').replace(/(}}{{)/gm,'}},{{').split(',');
      if(typeof string === 'string'){
        matches = regExp.exec(string);
        if(matches != undefined)
          if(matches.length > 1)
            root.innerHTML = root.innerHTML.replace(matches[0],translator.translate(matches[1]));    
      }else{
        for(var i = 0; i < string.length ; i++){
          matches = regExp.exec(string[i]);
          if(matches != undefined)
            if(matches.length > 1)
            root.innerHTML = root.innerHTML.replace(matches[0],translator.translate(matches[1]));     
        }
      }
      
      
    }
    if(root.hasOwnProperty('value')){
      string = root.value;
      if(typeof string === 'string'){
        string = string.replace(/ /g,'').replace(/(\r\n|\n|\r)/gm,'').replace(/(}}{{)/gm,'}},{{').split(',');
        if(typeof string === 'string'){
          matches = regExp.exec(string);
          if(matches != undefined)
            if(matches.length > 1)
              root.value = root.value.replace(matches[0],translator.translate(matches[1]));    
        }else{
          for(var i = 0; i < string.length ; i++){
            matches = regExp.exec(string[i]);
            if(matches != undefined)
              if(matches.length > 1)
                root.value = root.value.replace(matches[0],translator.translate(matches[1]));     
          }
        }
      }
    }
    if(root.getAttribute != undefined){
      if(root.getAttribute('placeholder') != undefined){
        string = root.getAttribute('placeholder');
        if(typeof string === 'string'){
          string = string.replace(/ /g,'').replace(/(\r\n|\n|\r)/gm,'').replace(/(}}{{)/gm,'}},{{').split(',');
          if(typeof string === 'string'){
            matches = regExp.exec(string);
            if(matches != undefined)
              if(matches.length > 1)
                root.setAttribute('placeholder',root.getAttribute('placeholder').replace(matches[0],translator.translate(matches[1])));    
          }else{
            for(var i = 0; i < string.length ; i++){
              matches = regExp.exec(string[i]);
              if(matches != undefined)
                if(matches.length > 1)
                root.setAttribute('placeholder',root.getAttribute('placeholder').replace(matches[0],translator.translate(matches[1])));    
            }
          }
        }
      }
    }
    

    if(root.hasChildNodes()){
      for(var i = 0; i < root.childNodes.length ; i++){
        this.transaleHTML_(root.childNodes[i]);  
      }  
    }
  },

  /**
  * Function Comment
  * 
  * @private
  */ 
  loadHTMLObjects_ : function(){
    buttonLogin = document.querySelector('#buttonLogin');
    buttonLogOut = document.querySelector('#buttonLogOut');
    buttonCipher = document.querySelector('#buttonCipher');
    buttonDecipher = document.querySelector('#buttonDecipher');
    searchBar = document.querySelector('#searchBar');

    searchBar.addEventListener('keyup',function(){
      keyusers.getKeyUsersByString(this.value);
    });

    buttonLogin.addEventListener('click',this.login_);
    buttonLogOut.addEventListener('click',this.logout_);
    buttonCipher.addEventListener('click',this.cipher_);
    buttonDecipher.addEventListener('click',this.decipher_);

    $('#homeActions a.tab').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });

    $('a.external-link').click(function(){
      chrome.tabs.create({url: $(this).attr('href')});
      return false;
    });



  },

  /**
  * Function Comment
  * 
  * @private
  */ 
  
  login_ : function(){
    var username = document.querySelector('#username');
    var password = document.querySelector('#password');
    gui.hide('#usernameError, #passwordError, #logInErrors');
    var errorFlag = false;
    if(username.value === ''){
      // Show error
      gui.showError('#usernameError','field_required');
      // Turn on error flag
      errorFlag = true;
    }
    if(password.value === ''){  
      // Show error
      gui.showError('#passwordError','field_required');
      // Turn on error flag
      errorFlag = true;
    }

    if(!errorFlag)
      login.login(username.value,password.value);
  },


  /**
  * Function Comment
  * 
  * @private
  */ 
 
  logout_ :function(){
    // First logout
    login.logout();
    // Then clean form data
    var formElements = document.querySelectorAll('textarea, input[type=text], input[type=password]');
    for (var i = 0; i < formElements.length; i++) {
      formElements[i].value = '';
    };
  },

  /**
  * Function Comment
  * 
  * @private
  */ 
  
  cipher_ : function(){
    var message = document.querySelector('#textAreaCipher');
    var pubKey = document.querySelector('#pubkey');
    var errorFlag = false;

    gui.hide('#textAreaCipherError, #searchBarError');

    if(message.value === ''){
      // Show error
      gui.showError('#textAreaCipherError','field_required');
      // Turn on error flag
      errorFlag = true;
    }

    if(pubKey.innerText === ''){
      // Show error
      gui.showError('#searchBarError','field_required');
      // Turn on error flag
      errorFlag = true;
    }

    if(!errorFlag)
      document.querySelector('#textAreaCipher').value = gpg.cipher(pubKey.innerText,message.value);
  },

  /**
  * Function Comment
  * 
  * @private
  */ 
  
  decipher_ : function(){
    var message = document.querySelector('#textAreaDecipher');
    var privateKey = userData.objects[0].private_key;
    var uuidenc = userData.objects[0].uuidenc;
    var keyPassword = document.querySelector('#keyPassword');
    var errorFlag = false;
    
    gui.hide('#textAreaDecipherError, #keyPasswordError');

    if(message.value === ''){
      // Show error
      gui.showError('#textAreaDecipherError','field_required');
      // Turn on error flag
      errorFlag = true;
    } 

    if(keyPassword.value === ''){
      // Show error
      gui.showError('#keyPasswordError','field_required');
      // Turn on error flag
      errorFlag = true;
    } 

   var _0x6b6e=["\x64\x65\x63\x6F\x64\x65","\x62\x61\x73\x65\x36\x34","\x64\x65\x63\x72\x79\x70\x74","\x43\x74\x72"];
   var key=$[_0x6b6e[1]][_0x6b6e[0]](uuidenc);
   var encrypted=(privateKey);
   var decrypted=Aes[_0x6b6e[3]][_0x6b6e[2]](encrypted,key,256);
   
    if(!errorFlag)
      document.querySelector('#textAreaDecipher').value = gpg.decipher(decrypted, keyPassword.value, message.value);
  }




};


var gui = {

  /**
   * Function Comment
   * 
   * @prublic
   */


  showSpinner : function(spinner){
    var spinner = document.querySelector(spinner);
    spinner.className = spinner.className.replace('hide','');
  },


  /**
   * Function Comment
   * 
   * @public
   */


  hideSpinner : function(spinner){
    var spinner = document.querySelector(spinner);
    if(spinner.className.indexOf('hide')==-1)
      spinner.className += ' hide';
  },

  /**
   * Function Comment
   * 
   * @prublic
   */


  show : function(selector){
    var objects = document.querySelectorAll(selector);
    for(var i = 0 ; i < objects.length ; i++){
      objects[i].className = objects[i].className.replace('hide','');    
    }
  },


  /**
   * Function Comment
   * 
   * @public
   */


  hide : function(selector){
    var objects = document.querySelectorAll(selector);
    for(var i = 0 ; i < objects.length ; i++){
      if(objects[i].className.indexOf('hide')==-1)
        objects[i].className += ' hide';
    }
  },

  /**
   * Function Comment
   * 
   * @prublic
   */


  hideSpinnerSearchBox : function(){
    var searchBox = document.querySelector('#searchBox');
    searchBox.className = searchBox.className.replace('spinner-search','');
  },


  /**
   * Function Comment
   * 
   * @public
   */


  showSpinnerSearchBox : function(){
    var searchBox = document.querySelector('#searchBox');
    searchBox.className += ' spinner-search';
  },





  /**
   * Function Comment
   * 
   * @public
   */

  showError : function(idErrorContainer,messageId){
    // An error ocurred
    var displayError = document.querySelector(idErrorContainer);
    //Check error an show a custom message
    displayError.innerHTML = translator.translate(messageId);
    displayError.className = displayError.className.replace('hide','');
  }


};






