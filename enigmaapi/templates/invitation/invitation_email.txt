{% load humanize %}
Someone, hopefully your friend, called {{ user_invite }}  invited you to communicate througth of secure communication with enigmagpg.com using this email address. If it was you, and you'd like to activate and use your account, click the link below or copy and paste it into your web browser's address bar:

https://app.enigmagpg.com/accounts/register

If you didn't request this, you don't need to do anything; you won't receive any more email from us.
