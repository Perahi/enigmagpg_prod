#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

from enigmaapi.apps.core.models import UserProfile, KeyContainer, InviteProfile
from django.views.generic import UpdateView
from enigmaapi.apps.core.forms import UserProfileForm, Request_passwordkey_form, UserInviteForm, UserMessageForm,\
    UserDecipherMessageForm
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import  render 
from datetime import datetime
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from tweepy.streaming import json
from django.http.response import Http404
import base64

# Show Decipher message form
@login_required
def view_form_message_decipher_create(request):
    form = UserDecipherMessageForm() # An unbound form
    userprofile = UserProfile.objects.get(user=request.user)
    privkey = userprofile.encrypt(userprofile.uuid_field,  userprofile.private_key)
    uuidenc = base64.b64encode(userprofile.uuid_field) 
    return render(request, 'core/decipherform.html', {'form': form,'userprofile':userprofile,'privkey':privkey,'uuidenc':uuidenc})    



# Show cipher message form
@login_required
def view_form_message_create(request,user_id):
    form = UserMessageForm() # An unbound form
    try: 
        keycontainers = KeyContainer.objects.get(user=UserProfile.objects.get(user=request.user),userContact__user=user_id)
        userprofile = UserProfile.objects.get(user=user_id)
        return render(request, 'core/cipherform.html', {'form': form,'userprofile':userprofile })    
    except KeyContainer.DoesNotExist,UserProfile.DoesNotExist:
        raise Http404  
        
    
    

# Show invitation user form mail
@login_required
def view_form_invite_user(request):
    form = UserInviteForm() # An unbound form
    return render(request, 'invitation/invitation_form.html', {'form': form, })    

@login_required
def ajax_keycontainer_search(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        results = KeyContainer.objects.filter(user=UserProfile.objects.get(user=request.user),userContact__user__first_name__contains=q)
        response = []
        for result in results:
            response_json = {}
            response_json['id'] = result.userContact.user.id
            response_json['label'] = result.userContact.user.last_name +', ' + result.userContact.user.first_name
            response_json['value'] = result.userContact.user.last_name +', ' + result.userContact.user.first_name
            response.append(response_json)
        data = json.dumps(response)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


@login_required
def ajax_get_keyPublic_userprofile(request,userprofile):
    results = []
    template = 'core/resultsearch.html'
    data = {'pubkey': results,}
    try: 
        if userprofile is not None:  
            results = UserProfile.objects.get(pk=userprofile)            
            return render_to_response(template, data, 
                context_instance = RequestContext(request))
    except KeyContainer.DoesNotExist,UserProfile.DoesNotExist:
        return render_to_response(template, data, 
                context_instance = RequestContext(request))

# Activate register user
@login_required
def activate_user(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = UserInviteForm(request.POST)
            if form.is_valid():
                # search profile user logged
                userprofile_logged = UserProfile.objects.get(user_id=request.user.id)
                ## Check if exist user mail
                try:
                    # search user mail in profile
                    userprofile = UserProfile.objects.get(email=form.data['email'])
                    # check if exist previously InviteProfile qith the same email for
                    # this user
                    try:
                        invite_users_search = InviteProfile.objects.get(userprofile=userprofile_logged, invitation_email=form.data['email'])         
                    except InviteProfile.DoesNotExist:
                        # exist in app and add contact
                        invite_users_search = InviteProfile()
                        # set user invite
                        invite_users_search.userprofile = userprofile_logged
                        # generate invitation_keys
                        invite_users_search.invitation_email = form.data['email']
                        invite_users_search.save()
                    invite_users_search.search_user_request_invited(userprofile, userprofile)
                    return HttpResponseRedirect('/user/invite/complete/')
                except UserProfile.DoesNotExist:
                    # check if exist previously InviteProfile qith the same email for
                    # this user
                    try:
                        user_invite = InviteProfile.objects.get(userprofile=userprofile_logged, invitation_email=form.data['email'])         
                    except InviteProfile.DoesNotExist:
                        ## create invite model and send mail 
                        user_invite = InviteProfile()
                        # search profile user logged
                        userprofile = UserProfile.objects.get(user_id=request.user.id)
                        # set user invite
                        user_invite.userprofile = userprofile
                        # generate invitation_keys
                        user_invite.invitation_email = form.data['email']
                        user_invite.save()
                    # send mail invitation
                    user_invite.send_activation_email(request.user,form)
                    return HttpResponseRedirect('/user/invite/complete/')
        else:
            form = UserInviteForm()
        return HttpResponseRedirect(redirect('invite_user',form))
    else:
        return HttpResponseRedirect(redirect('index'))

# Update info profile
@login_required
def save_profile(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = UserProfileForm(request.POST)
            if form.is_valid():
                # add firstname and lastname
                user = request.user
                user.last_name = form.data['last_name']
                user.first_name = form.data['first_name']
                user.save()
                form.save()
                return HttpResponseRedirect(redirect('index',form))
        else:
            form = UserProfileForm()
        return HttpResponseRedirect(redirect('userProfileView',form))
    else:
        return HttpResponseRedirect(redirect('index'))

# Show keypassword form
@login_required
def view_form_keypassword(request):
    form = Request_passwordkey_form() # An unbound form
    return render(request, 'home/request_password_keyprivate_form.html', {
        'form': form,
    })    

# Show user profile
@login_required
def view_user_profile(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            userprofile = UserProfile.objects.get(user_id=request.user.id) 
            form = UserProfileForm(request.POST,instance=userprofile)
            if form.is_valid():
                # add firstname and lastname
                user = request.user
                user.last_name = form.data['last_name']
                user.first_name = form.data['first_name']
                user.save()
                form.save()
                return HttpResponseRedirect('/')
            else:
                userprofile = UserProfile.objects.get(user_id=request.user.id) 
                contex = {'form' : form}
                return render(request,'home/edit_userprofile.html',contex) 
        else:    
            try:
                userprofile = UserProfile.objects.get(user_id=request.user.id)
                form = UserProfileForm(instance = userprofile)
                form.fields["last_name"].initial = request.user.last_name
                form.fields['first_name'].initial = request.user.first_name
                contex = {'form' : form}
                return render(request,'home/edit_userprofile.html',contex)
            except UserProfile.DoesNotExist:
                return HttpResponseRedirect('/keymanager/password/request')
    else:
        return redirect('auth_login')

# Show request renew keys force 
@login_required
def requestForceRenewKey(request):
    form = Request_passwordkey_form() # An unbound form
    return render(request, 'home/request_password_keyprivate_form_force.html', {
           'form': form,
       })    

# Complete action for renew key force
@login_required
def requestForceRenewKeyComplete(request):
    if request.user.is_authenticated():
        # verify user doesnt have userprofile and key profile
        try:
            # password to generate keys
            if 'keypassword' in request.POST:
                # verify if user exist in userprofile 
                userprofile = UserProfile.objects.get(user_id=request.user.id)
                # regenerate keys
                form = Request_passwordkey_form(request.POST) # A form bound to the POST data
                if form.is_valid(): # All validation rules pass}
                    keypassword = form.cleaned_data['keypassword']
                    # generate key pair user
                    result = userprofile.generateKeysPair(userprofile, keypassword, request.user.username, request.user.email)
                    
                    if not result=="generated_keys":
                        return render(request, 'home/request_password_keyprivate_form_force.html', {'errorgenerate': result})
           
                return redirect('index')
        except UserProfile.DoesNotExist:
            userprofile = None    
            return redirect('auth_login')
    else:
        userprofile = None    
        return redirect('auth_login')

# Request the keypassword 
@login_required
def requestkey(request):
    if request.user.is_authenticated():
        # verify user doesnt have userprofile and key profile
        try:
            # password to generate keys
            if 'keypassword' in request.POST:
                # verify if user exist in userprofile 
                userprofile = UserProfile.objects.get(user_id=request.user.id)
                # Verify if regenerate keys
                if userprofile.check_regenerateKeysDays():
                    # regenerate keys
                    form = Request_passwordkey_form(request.POST) # A form bound to the POST data
                    if form.is_valid(): # All validation rules pass}
                        keypassword = form.cleaned_data['keypassword']
                        # generate key pair user
                        result = userprofile.generateKeysPair(userprofile, keypassword, request.user.username, request.user.email)
                        
                        if not result=="generated_keys":
                            return render(request, 'home/request_password_keyprivate_form.html', {'errorgenerate': result})
                        
                return redirect('index')
        except UserProfile.DoesNotExist:
            form = Request_passwordkey_form(request.POST) # A form bound to the POST data
            if form.is_valid(): # All validation rules pass}
                keypassword = form.cleaned_data['keypassword']
                
                # New Profile
                userprofile = UserProfile()
                userprofile.email = request.user.email
                userprofile.membership = "U"
                userprofile.securitykeys = "D"
                userprofile.user= request.user
                userprofile.date_joined=datetime.now()
                userprofile.last_ip=request.META.get('REMOTE_ADDR')
                
                # generate key pair user
                result = userprofile.generateKeysPair(userprofile, keypassword, request.user.username, request.user.email)
                        
                if not result=="generated_keys":
                    # delete object
                    userprofile.delete()
                    return render(request, 'home/request_password_keyprivate_form.html', {'errorgenerate': result})  
                
                # verify invited user and setting in key container
                invite_users_search_pending = InviteProfile()
                invite_users_search_pending.search_user_request_invited(request.user, userprofile)
                
                # advise registered user in system
                invite_users_search_pending.send_activation_user(request.user)
                return redirect('index')
            else:
                return render(request, 'home/request_password_keyprivate_form.html', {
                'form': form,
            })  
    else:
        userprofile = None    
        return redirect('auth_login')
    
# Show home of user
def home(request):
    try:
        if request.user.is_authenticated():
            #queryset
            keycontainer = None
            userprofile = UserProfile.objects.get(user_id=request.user.id)
            keycontainer = KeyContainer.objects.all().filter(userContact=userprofile.id)
            # set IP address client
            userprofile.last_ip=request.META.get('REMOTE_ADDR')
            userprofile.save()
            # check regenerate keys pair
            if userprofile.check_regenerateKeysDays():
                return redirect('request_keypassword')
            # next days to regenerate keys
            daystoLeft = userprofile.get_nextGenerateKeysDays()
            
            # Contacts of user}
            results = KeyContainer.objects.filter(user=UserProfile.objects.get(user=request.user))
            
            
        else: 
            userprofile = None    
            return redirect('accounts/login')
    except UserProfile.DoesNotExist:
        if not request.user.is_anonymous():
            # is user logged but without profile and keycontainer -> generate
            return redirect('request_keypassword')
        else:
            userprofile = None    
            return redirect('auth_login')
    context = {'userprofile': userprofile,'keycontainer':keycontainer,'daystoleft':daystoLeft,'contacts':results}
   
            
    return render(request,'index.html', context)

# Update user profile
@login_required
class UpdateUserProfileView(UpdateView):
    template_name = 'home/edit_userprofile.html'
    form_model      = UserProfile
    form_class = UserProfileForm
    
    # data of user logged
    def get_queryset(self):
        # get profile DB
        userProfile = UserProfile.objects.filter(user_id=self.request.user.id)
        # SET PK Form
        self.kwargs['pk'] = self.kwargs.get('pk', userProfile[0].id)
        
        return userProfile