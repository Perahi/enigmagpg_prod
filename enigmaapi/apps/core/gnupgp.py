#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

from enigmaapi.apps.core.GPGModules import key_management
from enigmaapi import settings

class gpgBackend:

    def generate_multiple_key(self, securitykeys, password,name,email):
        # Default
        days = "365d"
        if securitykeys=="I":
            days = "7d"
        elif securitykeys == "P":
            days = "1d"
            
        keytype = settings.GNUPG_KEY_TYPE
        keylength = settings.GNUPG_KEY_LENGTH
        comment = settings.GNUPG_COMMENT
        subkeytype = settings.GNUPG_SUBKEY_TYPE
        subkeylength = settings.GNUPG_SUBKEY_LENGTH  
            
        result = key_management.generate_multiple_key(keytype,keylength,days,password, name, comment, email,subkeytype,subkeylength)
        return result
    
    def get_key_to_text(self,email,private):
        checks_list  = key_management.export_key_to_text(email, private)
        return checks_list.get_checks_list().get('result').get('data')
    
    def delete_key(self,email,private):
        key_management.delete_key(email, private)
