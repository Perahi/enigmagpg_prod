# -*- coding: utf-8 -*-

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg


## @module verify sign
#  This script has methods to manage sign verification.

import gnupg
import json
import sys
#import ast
#from pprint import pprint

sys.path.append("../Extras")
from ..Extras.checks_list import ChecksList

sys.path.append("../Extras")
from ..Extras.helper_functions import *

try:
	gpg = gnupg.GPG()
except ValueError as error:
	print "GPG application is not installed"
	print error
	sys.exit()

## Method that verifies a signed text
#  @param text Text to verify its sign
#  @result Operation status
def verify_text(text):

	checks_list = ChecksList()

	try: 
		result = gpg.verify(text)
		if not result:
			if ((result.username) and (result.key_id)) or (not(result.username) and not(result.key_id)):
				## Verification failed, the key is wrong
				checks_list.add_error('no_verify')
			else:
				## The key doesn't exist
				checks_list.add_error('missing_key')
				
		checks_list.add_result('status',  result.status)
		checks_list.add_result('valid',  result.valid)
		checks_list.add_result('fingerprint', result.fingerprint)
		checks_list.add_result('signature_id', result.signature_id)
		checks_list.add_result('username', result.username)
		checks_list.add_result('trust_text', result.trust_text)
		checks_list.add_result('trust_level', result.trust_level)
		checks_list.add_result('key_id', result.key_id)
		checks_list.add_result('pubkey_fingerprint', result.pubkey_fingerprint)
		checks_list.add_result('expire_timestamp', result.expire_timestamp)
		checks_list.add_result('sig_timestamp', result.sig_timestamp)
	
	except :
		checks_list.add_error('unknown_error')

	return checks_list

## Method that verifies a signed file
#  @param input_file_path Signed file's path 
#  @result Operation status
def verify_file(input_file_path):

	checks_list = ChecksList()

	try:
		stream = open(input_file_path, "rb")
		try:
			result = gpg.verify_file(stream)
			if not result:
				if ((result.username) and (result.key_id)) or (not(result.username) and not(result.key_id)):
					## Verification failed, the key is wrong
					checks_list.add_error('no_verify')
				else:
					## The key doesn't exist
					checks_list.add_error('missing_key')
			
			checks_list.add_result('status',  result.status)
			checks_list.add_result('valid',  result.valid)
			checks_list.add_result('fingerprint', result.fingerprint)
			checks_list.add_result('signature_id', result.signature_id)
			checks_list.add_result('username', result.username)
			checks_list.add_result('trust_text', result.trust_text)
			checks_list.add_result('trust_level', result.trust_level)
			checks_list.add_result('key_id', result.key_id)
			checks_list.add_result('pubkey_fingerprint', result.pubkey_fingerprint)
			checks_list.add_result('expire_timestamp', result.expire_timestamp)
			checks_list.add_result('sig_timestamp', result.sig_timestamp)

		except :
			checks_list.add_error('unknown_error')
	except :
		checks_list.add_error('input_file_error')

	return checks_list
