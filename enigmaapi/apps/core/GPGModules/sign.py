# -*- coding: utf-8 *-*

#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Authors                                                             
# Mailen Horbulewicz, mailen.horbul@gmail.com
# Maximo Martinez, maximomrtnz@gmail.com
# Pablo Meyer, pablitomeyer@gmail.com
# 
# Graduation Project at FASTA University - Mar del Plata, Argentina
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of enigma-gpg: http://www.talsoft.com.ar/index.php/research/tools/enigma-gpg


## @module sign
#  This script has methods to manage signing

import gnupg
import re
import json
import sys
#import ast
#from pprint import pprint

sys.path.append("../Extras")
from ..Extras.checks_list import ChecksList

## Validations
sys.path.append("../Extras")
from ..Extras.helper_functions import *

try:
	gpg = gnupg.GPG()
except ValueError as error:
	print "GPG application is not installed"
	print error
	sys.exit()

## Method that signs the text passed as a parameter
#  @param text Text to sign
#  @param email Sender's email (Key to sign the text).
#  @param password Sender key's password 
#  @result Signed text
def sign_text(text, email, password):        
	
	checks_list = ChecksList()
	checks_list.add_validation('email',is_email_ok(email))
	
	if checks_list.is_valid() :
		try:
			result = gpg.sign(text, keyid = email, passphrase = password)
			if result:
				checks_list.add_result('data', result.data + ' Generated by Enigma GPG ')
			else :
				## The email isn't valid or the password is wrong
				checks_list.add_error('no_sign')

		except :
			## An error ocurred
			checks_list.add_error('unknown_error')
		
	return checks_list

## Method that signs the text passed as a parameter and returns it stored in a file
#  @param text Text to sign
#  @param output_file_path The File's path were the text will be stored
#  @param email Sender's email(key to sign the text)
#  @param password Sender key's password 
#  @result Signed text
def sign_text_to_file(text, output_file_path, email, password):

	checks_list = ChecksList()
	checks_list.add_validation('email',is_email_ok(email))

	if checks_list.is_valid() :
		try:
			result = gpg.sign(text, keyid = email, passphrase = password)
			if result :
				try :
					output = open(output_file_path, 'w')
					output.write(result.data)
					checks_list.add_result('status','sign ok')
					checks_list.add_result('data',output_file_path)
				except :
					## An error ocurred trying to create the output file
					checks_list.add_error('output_file_error')
			else :
				## The email isn't valid or the password is wrong
				checks_list.add_error('no_sign')
		except :
			## An error ocurred
			checks_list.add_error('unknown_error')
			
	return checks_list

## Method that signs the file passed as a parameter
#  @param input_file_path File to sign
#  @param output_file_path The File's path were the text will be stored
#  @param email Sender's email(key to sign the text)
#  @param password Sender key's password 
#  @result Signed file
def sign_file(input_file_path, output_file_path, email, password):
	
	checks_list = ChecksList()
	checks_list.add_validation('email',is_email_ok(email))

	if checks_list.is_valid() :
		try:
			stream = open(input_file_path, "rb")
			result = gpg.sign_file(stream, keyid = email, passphrase = password)
			if result:
				try:
					output = open(output_file_path, 'w')
					output.write(result.data)
					checks_list.add_result('status','sign ok')
					checks_list.add_result('data',output_file_path)
				except:
					checks_list.add_error('output_file_error')
			else:	
				## The email isn't valid or the password is wrong
				checks_list.add_error('no_sign')
		except IOError:
			## An error ocurred trying to open the file
			checks_list.add_error('input_file_error')
		except :
			## An error ocurred
			checks_list.add_error('unknown_error')
			
	return checks_list


## Method that signs the file passed as a parameter and returns it as text
#  @param input_file_path File to sign
#  @param email Sender's email(key to sign the text)
#  @param password Sender key's password 
#  @result Signed text
def sign_file_to_text(input_file_path, email, password):
		
	checks_list = ChecksList()
	checks_list.add_validation('email',is_email_ok(email))

	if checks_list.is_valid() :
		try:
			stream = open(input_file_path, "rb")
			result = gpg.sign_file(stream, keyid = email, passphrase = password)
			if result :
				checks_list.add_result('status','sign ok')
				checks_list.add_result('data', unicode(str(result.data) + ' Generated by Enigma GPG ', errors='ignore'))
			else :
				checks_list.add_error('no_sign')			
				
		except IOError:
			## An error ocurred trying to open the file
			checks_list.add_error('input_file_error')
		except :
			## An error ocurred
			checks_list.add_error('unknown_error')
				
	return checks_list
