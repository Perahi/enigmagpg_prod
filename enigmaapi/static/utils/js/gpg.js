/**  Copyright (C) 2013  Authors, TALSOFT S.R.L.
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 
 TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
 See the site of Enigma-gpg: http://www.enigmagpg.com
	
*/

function encrypt() {
  if (window.crypto.getRandomValues) {
    require("/static/utils/js/openpgp.js");
    openpgp.init();
    var pub_key = openpgp.read_publicKey($('#pubkey').val());
    var generated = "Generated by Enigma GPG - www.enigmagpg.com";
    $('#id_message').val(openpgp.write_encrypted_message(pub_key,$('#id_message').val())+"  \r\n"+generated);
    return true;
  } else {
    window.alert("Error: Browser not supported\nReason: We need a cryptographically secure PRNG to be implemented (i.e. the window.crypto method)\nSolution: Use Chrome >= 11, Safari >= 3.1 or Firefox >= 21");   
    return true;
  }
}

function dencrypt(){
	if (window.crypto.getRandomValues) {
	    require("/static/utils/js/openpgp.js");
	    require("/static/utils/js/aes2.js");
	    require("/static/utils/js/base64.js");
	     
	    var key = $.base64.decode($('#uuidenc').val());
	    var encrypted = ($('#privkey').val());
	    var decrypted = Aes.Ctr.decrypt(encrypted, key, 256);
	    
	    /*  Obfuscate decipher
	     * var _0x8215=["\x76\x61\x6C","\x23\x75\x75\x69\x64\x65\x6E\x63","\x64\x65\x63\x6F\x64\x65","\x62\x61\x73\x65\x36\x34","\x23\x70\x72\x69\x76\x6B\x65\x79","\x64\x65\x63\x72\x79\x70\x74","\x43\x74\x72"];var key=$[_0x8215[3]][_0x8215[2]]($(_0x8215[1])[_0x8215[0]]());var encrypted=($(_0x8215[4])[_0x8215[0]]());var decrypted=Aes[_0x8215[6]][_0x8215[5]](encrypted,key,256);
	     * */
	    
	    openpgp.init();
	    var privKey = openpgp.read_privateKey(decrypted);
	   
	    var keyPassword = $('#id_keypassword').val()
		if (privKey.length < 1) {
			window.alert("No private key found!");
			return;
		}
	    
	    var temp = $('#id_message').val();
	    var str = temp.replace("Generated by Enigma GPG - www.enigmagpg.com","");
	    var msg = openpgp.read_message(str);
	    
	    var keymat = null;
		var sesskey = null;
		// Find the private (sub)key for the session key of the message
		for (var i = 0; i< msg[0].sessionKeys.length; i++) {
			if (privKey[0].privateKeyPacket.publicKey.getKeyId() == msg[0].sessionKeys[i].keyId.bytes) {
				keymat = { key: privKey[0], keymaterial: privKey[0].privateKeyPacket};
				sesskey = msg[0].sessionKeys[i];
				break;
			}
			for (var j = 0; j < privKey[0].subKeys.length; j++) {
				if (privKey[0].subKeys[j].publicKey.getKeyId() == msg[0].sessionKeys[i].keyId.bytes) {
					keymat = { key: privKey[0], keymaterial: privKey[0].subKeys[j]};
					sesskey = msg[0].sessionKeys[i];
					break;
				}
			}
		}
		if (keymat != null) {
			if (!keymat.keymaterial.decryptSecretMPIs(keyPassword)) {
				window.alert( "Password for secrect key was incorrect!");
				return;

			}
			$('#id_message').val(msg[0].decrypt(keymat, sesskey));
		    return true;
		}else {
			window.alert("No private key found!");
		}
		
	    return true;
	  } else {
	    window.alert("Error: Browser not supported\nReason: We need a cryptographically secure PRNG to be implemented (i.e. the window.crypto method)\nSolution: Use Chrome >= 11, Safari >= 3.1 or Firefox >= 21");   
	    return true;
	  }
	
}

function require(script) {
    $.ajax({
        url: script,
        dataType: "script",
        async: false,           // <-- this is the key
        success: function () {
            // all good...
        },
        error: function () {
            throw new Error("Could not load script " + script);
        }
    });
}