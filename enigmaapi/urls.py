#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com

#from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf.urls.static import static
from enigmaapi.apps.core.models import KeyContainer, UserProfile, InviteProfile
from enigmaapi.apps.core import views
from enigmaapi.apps.core.forms import RegistrationViewUniqueEmail
from django.contrib.auth import views as auth_views

admin.autodiscover()
admin.site.register(UserProfile)
admin.site.register(KeyContainer)
admin.site.register(InviteProfile)

from tastypie.api import Api
from api import UserResource,KeyContainerResource
 
v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(KeyContainerResource())


urlpatterns = patterns('',
    # Apps Core urls
    url(r'^',include('enigmaapi.apps.core.urls')),
    url(r'^oauth2/', include('provider.oauth2.urls', namespace = 'oauth2')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
     #override the default urls
    url(r'^accounts/password/change/$', auth_views.password_change, name='password_change'),
    url(r'^accounts/password/change/done/$',  auth_views.password_change_done,  name='password_change_done'),
    url(r'^accounts/password/reset/$', auth_views.password_reset,  name='password_reset'),
    url(r'^accounts/password/reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^accounts/password/reset/complete/$', auth_views.password_reset_complete,  name='password_reset_complete'),
    url(r'^accounts/password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',  auth_views.password_reset_confirm, name='password_reset_confirm'),

    
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^user/invite/complete/$', TemplateView.as_view(template_name='invitation/invitation_complete.html'),name='complete_invite_user'),
#    (r'^accounts/', include('allauth.urls')),
    # Unique mail and username in register accounts and strong password
    url(r'^accounts/register', RegistrationViewUniqueEmail.as_view(), name='registration_register'),
)
