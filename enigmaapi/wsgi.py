#  Copyright (C) 2013  Authors, TALSOFT S.R.L.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# 
# TALSOFT S.R.L. - www.talsoft.com.ar - Mar del Plata, Argentina
# See the site of Enigma-gpg: http://www.enigmagpg.com
"""
WSGI config for enigmaapi project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.
In prod descomment #
"""
import os
import sys
import os, pwd

path = '/var/www/app.enigmagpg.com/enigmagpg'

if path not in sys.path:
    sys.path.insert(0, '/var/www/app.enigmagpg.com/enigmagpg')
    
os.environ["HOME"] = pwd.getpwuid(os.getuid()).pw_dir
os.environ["USERNAME"] = "ubuntu"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "enigmaapi.settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
